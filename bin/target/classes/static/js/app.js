var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    //$("#greetings").html("");
}

function connect() {
    var socket = new SockJS('/gs-guide-websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/greetings', function (greeting) {  
        	console.log('greeting (with .body)= ' + greeting.body);
        	console.log('greeting(without .body)= ' + greeting);

        	showGreeting(greeting.body);
        });
    });
}
//showGreeting(JSON.parse(greeting.body).content);
function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendName() {
    stompClient.send("/app/hello", {}, JSON.stringify({'name': $("#name").val()}));
}

function showGreeting(message) {
	
	var list=message;
	console.log('list: '+list);
	var parsedList= JSON.parse(message);
	console.log('parsedlist: '+parsedList);

	//var json=[0]list;
	//console.log('list[0]'+list.id);
	$("#greetings").html("")
	Object.keys(parsedList).forEach(key => {
	    console.log(parsedList[key]);
	    /*
	    $("#message").append(parsedList[key].id+"<br>");
	    $("#message").append(parsedList[key].ipv6Address+"<br>");
	    $("#message").append(parsedList[key].type+"<br>");
	    $("#message").append(parsedList[key].value+"<br>");
	    $("#message").append(parsedList[key].metricSystem+"<br>");
	    $("#message").append(parsedList[key].location+"<br>");
	    $("#message").append(parsedList[key].dateTime+"<br>");

	   */
	   // $("#conversation").append(parsedList[key].id+"<br>");
	   // $("#conversation").append("<tr>");
	    /*
	    $("#conversation").append("<tr><td>"+parsedList[key].ipv6Address+"</td>");
	    $("#conversation").append("<td>"+parsedList[key].type+"</td>");
	    $("#conversation").append("<td>"+parsedList[key].value+"</td>");
	    $("#conversation").append("<td>"+parsedList[key].metricSystem+"</td>");
	    $("#conversation").append("<td>"+parsedList[key].location+"</td>");
	    $("#conversation").append("<td>"+parsedList[key].dateTime+"</td></tr>");*/
	  //  $("#conversation").append("</tr>");
	//    $("#greetings").append("<tr><td>"+parsedList[key].sensorId+"</td>"+
	    
	       $("#greetings").append("<tr><td><a href=viewSensor?id="+parsedList[key].sensorId+"&statsFor=1><i>"+ parsedList[key].sensorId+"</i></a></td>"+
	    							  "<td>"+parsedList[key].ipv6Address+"</td>"+
	    							  "<td>"+parsedList[key].port+"</td>"+
	    				              "<td>"+parsedList[key].type+"</td>"+
	    				              "<td>"+parsedList[key].value+"</td>"+
	    				              "<td>"+parsedList[key].metricSystem+"</td>"+
	    				              "<td>"+parsedList[key].location+"</td>"+
	    				              "<td>"+parsedList[key].dateTime+"</td>"+
	    							  "<td>"+parsedList[key].status+"</td></tr>");
	    				             /* "<td>"+dateFormat(parsedList[key].dateTime,"ddd, mmm dS, yyyy, h:MM:ss")+"</td></tr>");*/
	    //dateFormat(now, "dddd, mmmm dS, yyyy, h:MM:ss TT")
	    
	   // console.log(parsedList[key.id];)
	  });
	//console.log('message length='+([0]list).IPv6Address);
//	$("#greetings").append("<tr><td>" +message+ "</td></tr>");
  //$("#greetings").append("<tr><td> <c:out value="${'<b>This is a </b>'}"/> </td></tr>");
	//$("#message")
	
	//$("#message").html(message);
   // window.location.reload(true);
  testTheMonitor();
	
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendName(); });
});

