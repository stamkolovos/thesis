<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
<%@page isELIgnored="false" %>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" >
<title>Insert title here</title>

</head>
<body>
<h2>Real-time Monitor Devices:</h2>
<table border=1>
<tr>
<th>IPv6Address </th>
<th>Type </th>
<th>Value </th>
<th>Metric System </th>
<th>Location </th>
<th>DateTime </th>
</tr>


<!-- monitorList  einai auto pou erxetai apo ton controller, to monitorListItem einai mia metavlhth gia kathe stoixeio ksexwrista  -->

<c:forEach items="${monitorList}" var="monitorListItem">
<tr>
<td> ${monitorListItem.IPv6Address}</td>
<td>${monitorListItem.type}</td>
<td>${monitorListItem.value}</td>
<td>${monitorListItem.metricSystem}</td>
<td>${monitorListItem.location}</td>
<td>${monitorListItem.dateTime}</td>




</tr>
</c:forEach>
</table>

<a href="showCreate">Add Device</a> <br>
<a href="index">Go to index page</a> <br> <br>

            
<button onclick="myFunction()">Reload page</button>

<script>
function myFunction() {
    location.reload();
}
</script>
</body>
</html>