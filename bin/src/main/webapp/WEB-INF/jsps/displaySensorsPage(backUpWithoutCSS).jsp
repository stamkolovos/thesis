<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
<%@page isELIgnored="false" %>



<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" >
<title>Display Sensors</title>
</head>
<body>
<h2>Sensors:</h2>
<table>
<tr>
<th>Id </th>
<th>IPv6Address </th>
<th>Type</th>
<th>Metric_System  </th>
<th> Location</th>
<th>Resource_Path </th>
</tr>
<c:forEach items="${allSensors}" var="allSensors">
<tr>
<td> ${allSensors.id}</td>
<td> ${allSensors.IPv6Address}</td>
<td> ${allSensors.type}</td>
<td> ${allSensors.metricSystem}</td>
<td> ${allSensors.location}</td>
<td> ${allSensors.resourcePath}</td>
<td> <a href="editSensor?id=${allSensors.id}">Edit</a></td>
<td> <a href="deleteSensor?id=${allSensors.id}">Delete</a></td>
</tr>
</c:forEach>


</table>
<a href="addSensor">Add Device</a> <br>
<a href="monitorSensors">Monitor Sensors</a> <br>
<a href="index">Go to index page</a>

<button onclick="myFunction()">Reload page</button>

<script>
function myFunction() {
    window.location.reload(true);
}
</script>
</body>
</html>