<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
<%@page isELIgnored="false" %>



<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" >
<title>Display Sensors</title>
<link rel="stylesheet" href="/webjars/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="/css/main.css" rel="stylesheet">
        <link href="/css/displaySensorsPageStyle.css" rel="stylesheet">
    
    <script src="/webjars/jquery/3.1.0/jquery.min.js"></script>
    <script src="/webjars/sockjs-client/1.0.2/sockjs.min.js"></script>
    <script src="/webjars/stomp-websocket/2.3.3/stomp.min.js"></script>
    <script src="/webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="/js/app.js"></script>
</head>
<body>
<div id="main-content" class="container">
<ul>
	 
  <li><a href="/addSensor">Add new Sensor</a>  </li>
 <li> <a href="/monitorSensors">Monitor Sensors</a></li>
 <li> <a href="/">Index</a> </li>
  
</ul>
<hr/>
<h1>~ Display all Sensors ~</h1>
<div class="div-tableAllSensors">
<table>
<thead>
<tr>
<th>Id </th>
<th>IPv6Address </th>
<th>Port </th>

<th>Type</th>
<th>Metric_System  </th>
<th> Location</th>
<th>Resource_Path </th>
</tr>
</thead>
<tbody>
<c:forEach items="${allSensors}" var="allSensors">

<tr>

<td> ${allSensors.id}</td>
<td> ${allSensors.IPv6Address}</td>
<td> ${allSensors.port}</td>
<td> ${allSensors.type}</td>
<td> ${allSensors.metricSystem}</td>
<td> ${allSensors.location}</td>
<td> ${allSensors.resourcePath}</td>
<td> <a  href="viewSensor?id=${allSensors.id}&statsFor=1"><i>View-</i></a>
	<a  href="editSensor?id=${allSensors.id}"><i>Edit-</i></a>
	<a href="deleteSensor?id=${allSensors.id}"><i>Delete</i></a>
</td>

<!-- 
<td> <a  href="editSensor?id=${allSensors.id}"><i>Edit</i></a></td>
<td> <a href="deleteSensor?id=${allSensors.id}"><i>Delete</i></a></td>
-->

</tr>
</c:forEach>
</tbody>
</table>
</div>
<!-- 
<button onclick="myFunction()">Reload page</button>

<script>
function myFunction() {
    window.location.reload(true);
}
</script>
-->

</div>
</body>
</html>