<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Edit Sensor</title>
</head>
<body>


<form action="saveSensor" method="post">
<pre>
id(readonly):<input type="text" name="id" value="${editedSensor.id}" readonly/>
IPv6Address:<input type="text" name="IPv6Address" value="${editedSensor.IPv6Address}"/> 
Type:<input type="text" name="type" value="${editedSensor.type}"/>
Metric System:<input type="text" name="metricSystem" value="${editedSensor.metricSystem}"/> 
Location:<input type="text" name="location" value="${editedSensor.location}"/>
Resource Path: <input type="text" name="resourcePath" value="${editedSensor.resourcePath}"/>
<input type="Submit" value="save"/>
</pre>
</form>

${savedMessage} <br>

<a href="monitorSensors">Monitor Devices</a> <br>
<a href="displaySensors"> View All Devices</a> <br>
<a href="index">Go to index page</a>

</body>
</html>