<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Manage the Wireless Sensor Network</title>
</head>
<body>
<h2>Manage the Wireless Sensor Network:</h2>
<a href="addSensor"> Add Sensor</a> <br>
<a href="monitorSensors">Monitor Sensors</a> <br>
<a href="displaySensors"> View All Sensors</a>

</body>
</html>