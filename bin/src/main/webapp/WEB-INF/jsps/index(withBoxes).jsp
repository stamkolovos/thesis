<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="com.thesis.wsn.webUI.controller.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>

<head>
    <title>Welcome page</title>
 
    <link rel="stylesheet" href="/webjars/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="/css/main.css" rel="stylesheet">
        <link href="/css/indexStyle.css" rel="stylesheet">
    
    <script src="/webjars/jquery/3.1.0/jquery.min.js"></script>
    <script src="/webjars/sockjs-client/1.0.2/sockjs.min.js"></script>
    <script src="/webjars/stomp-websocket/2.3.3/stomp.min.js"></script>
    <script src="/webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="/js/app.js"></script>
    <script>
    function testTheMonitor(){
    	// 
    	//alert("bingooo");
    	//window.location.reload(true);
    	 <%//request.setAttribute("monitorList",SensorController.testCallJavaFromJSP()); %>
    	 
    }
    
    
    </script>
</head>
<body>
<noscript><h2 style="color: #ff0000">Seems your browser doesn't support Javascript! Websocket relies on Javascript being
    enabled. Please enable
    Javascript and reload this page!</h2></noscript>
    <div id="gap" class="top-gap">
    </div>
   <div id="main-content" class="top-container">
   <h1>
<i>~ Index of Wireless Sensor Network ~</i>
</h1>
   </div>
<!-- <div id="gap" class="middle-gap">
</div> -->

<!--  <div id="main-content" class="box-addsensor"> -->
<div>
<figure class="figure-addsensor">

<figcaption>Add new Sensor:</figcaption>

<a href="addSensor">
<img src="/images/add_sensor.png" alt="add_new_sensor" style="width:250px;height:145.781px;">

</a>
</figure>
<!-- </div> -->
<!-- <div id="main-content" class="container"> -->



<figure class="figure-displaysensors">

<figcaption>Display Sensors:</figcaption>

<a href="displaySensors">
<img src="/images/display_sensors.png" alt="display_sensors" style="width:30but0px;height:170px;">

</a>
</figure>
<figure class="figure-monitorsensors">

<figcaption>Monitor Sensors:</figcaption>

<a href="monitorSensors">
<img src="/images/monitor_sensors.png" alt="monitor_sensors" style="width:250px;height:145.781px;">

</a>
</figure>
</div>

<div>

</div>
 
<!-- </div> -->
</body>
</html>
