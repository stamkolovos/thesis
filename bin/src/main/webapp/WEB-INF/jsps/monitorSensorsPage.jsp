<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="com.thesis.wsn.webUI.controller.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>

<head>
    <title>Monitor Sensors</title>
 
    <link rel="stylesheet" href="/webjars/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="/css/main.css" rel="stylesheet">
        <link href="/css/monitorSensorPageStyle.css" rel="stylesheet">
    
    <script src="/webjars/jquery/3.1.0/jquery.min.js"></script>
    <script src="/webjars/sockjs-client/1.0.2/sockjs.min.js"></script>
    <script src="/webjars/stomp-websocket/2.3.3/stomp.min.js"></script>
    <script src="/webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="/js/app.js"></script>
    <script>
    function testTheMonitor(){
    	// 
    	//alert("bingooo");
    	//window.location.reload(true);
    	 <%//request.setAttribute("monitorList",SensorController.testCallJavaFromJSP()); %>
    	 
    }
    
    
    </script>
</head>
<body>
<noscript><h2 style="color: #ff0000">Seems your browser doesn't support Javascript! Websocket relies on Javascript being
    enabled. Please enable
    Javascript and reload this page!</h2></noscript>
      
<div id="main-content" class="container">


<ul>
	 
  <li><a href="/addSensor">Add new Sensor</a>  </li>
 <li> <a href="/displaySensors">Display Sensors</a>  </li>
 <li> <a href="/">Index</a> </li>
  
</ul>
<hr/>
  <div class="row">
        <div class="col-md-6">
            <form class="form-inline">
                <div class="form-group">
                
                    <label for="connect">Monitor Wireless Sensor Network:</label>
                    <button id="connect" class="btn btn-default" type="submit">Start</button>
                    <button id="disconnect" class="btn btn-default" type="submit" disabled="disabled">Stop
                    </button>
                </div>
            </form>
        </div>
        
      <!--   <div class="col-md-6">
            <form class="form-inline">
                <div class="form-group">
                    <label for="name">What is your name?</label>
                    <input type="text" id="name" class="form-control" placeholder="Your name here...">
                </div>
                <button id="send" class="btn btn-default" type="submit">Send</button>
            </form>
        </div>
       -->
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="conversation" class="table table-striped">
                <thead>
                <tr>
                	<th>Sensor id </th>
					<th>IPv6Address </th>
					<th>Port </th>
					
					<th>Type </th>
					<th>Value </th>
					<th>Metric System </th>
					<th>Location </th>
					<th>DateTime </th>
					<th>Status</th>
				</tr>
                </thead>
                <tbody id="greetings">
                </tbody>
                
				
				
                
               
				




            </table>
        </div>
    </div>
</div>
</body>
</html>
