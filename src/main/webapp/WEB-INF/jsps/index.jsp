<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="com.thesis.wsn.webUI.controller.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>

<head>
    <title>Welcome page</title>
 
    <link rel="stylesheet" href="/webjars/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="/css/main.css" rel="stylesheet">
        <link href="/css/indexStyle.css" rel="stylesheet">
    
    <script src="/webjars/jquery/3.1.0/jquery.min.js"></script>
    <script src="/webjars/sockjs-client/1.0.2/sockjs.min.js"></script>
    <script src="/webjars/stomp-websocket/2.3.3/stomp.min.js"></script>
    <script src="/webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="/js/app.js"></script>
    <script>
    function testTheMonitor(){
    	// 
    	//alert("bingooo");
    	//window.location.reload(true);
    	 <%//request.setAttribute("monitorList",SensorController.testCallJavaFromJSP()); %>
    	 
    }
    
    
    </script>
</head>
<body>
<noscript><h2 style="color: #ff0000">Seems your browser doesn't support Javascript! Websocket relies on Javascript being
    enabled. Please enable
    Javascript and reload this page!</h2></noscript>
   <div class="top-gap">
   </div>
   <div id="main-content" class="top-container">
   <h1>
<i>~ Index of Wireless Sensor Network ~</i>
</h1>
   </div>
<!-- <div id="gap" class="middle-gap">
</div> -->

<!--  <div id="main-content" class="box-addsensor"> -->
<div class="second-gap">
   </div>
<table>
<tr>
<td>
<figure class="figure-addsensor">

<figcaption>Add new Sensor:</figcaption>

<a href="addSensor">
<img class="img-addsensor" src="/images/add_sensor.png" alt="add_new_sensor" >

</a>
</figure>
</td>
<!-- </div> -->
<!-- <div id="main-content" class="container"> -->
<td class="td-gap"></td>
<td>

<figure class="figure-displaysensors">

<figcaption>Display Sensors:</figcaption>

<a href="displaySensors">
<img class="img-displaysensors" src="/images/display_sensors.png" alt="display_sensors" >

</a>
</figure>
</td>
<td class="td-gap"></td>

<td>
<figure class="figure-monitorsensors">

<figcaption>Monitor Sensors:</figcaption>

<a href="monitorSensors">
<img class="img-monitorsensors" src="/images/monitor_sensors.png" alt="monitor_sensors">

</a>
</figure>
</td>
</table>




 
<!-- </div> -->
</body>
</html>
