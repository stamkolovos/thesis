<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Add Sensor</title>
    <link rel="stylesheet" href="/webjars/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="/css/main.css" rel="stylesheet">
        <link href="/css/addSensorPage.css" rel="stylesheet">
    

</head>
<body>
<noscript><h2 style="color: #ff0000">Seems your browser doesn't support Javascript! Websocket relies on Javascript being
    enabled. Please enable
    Javascript and reload this page!</h2></noscript>
    
<div id="main-content" class="container">


<ul>
	 
  <li><a href="/monitorSensors"> Monitor Sensors</a>  </li>
 <li> <a href="/displaySensors">Display Sensors</a>  </li>
 <li> <a href="/">Index</a> </li>
  
</ul>
<hr/>


<h1>~ Add new Sensor ~</h1>
<div class="div-input-form">

<form class="input-form" action="saveSensor" method="post">
<div class="form-row">
<label for="IPv6Address">IPv6Address:</label><input id="IPv6Address" type="text" name="IPv6Address"/> 
</div>
<div class="form-row">
<label for="port">Port:</label><input id="port" type="text" name="port"/>
</div>
<div class="form-row">
<label for="type">Type:</label><input id="type" type="text" name="type"/>
</div>
<div class="form-row">
<label for="metricSystem">Metric System:</label><input id="metricSystem" type="text" name="metricSystem"/> 
</div>
<div class="form-row">
<label for="location">Location:</label><input id="location" type="text" name="location"/>
</div>
<div class="form-row">
<label for="resourcePath">Resource Path:</label><input id="resourcePath" type="text" name="resourcePath"/>
</div>

<div class="class-saveButton">
<input id="saveButton" type="Submit" value="Save"/>
</div>
${savedMessage}
</form>
</div>

 <br>


</div>
</body>
</html>