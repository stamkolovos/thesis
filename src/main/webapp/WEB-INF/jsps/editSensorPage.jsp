<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Edit Sensor</title>
    <link rel="stylesheet" href="/webjars/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="/css/main.css" rel="stylesheet">
        <link href="/css/editSensorPageStyle.css" rel="stylesheet">
    

</head>
<body>
<noscript><h2 style="color: #ff0000">Seems your browser doesn't support Javascript! Websocket relies on Javascript being
    enabled. Please enable
    Javascript and reload this page!</h2></noscript>
    
<div id="main-content" class="container">


<ul>
	 
  <li><a href="/monitorSensors"> Monitor Sensors</a>  </li>
 <li> <a href="/displaySensors">Display Sensors</a>  </li>
 <li> <a href="/">Index</a> </li>
  
</ul>
<hr/>


<h1>~ Edit Sensor ~</h1>
<div class="div-input-form">

<form class="input-form" action="saveSensor" method="post">
<div class="form-row">
<label for="id">Id:</label><input id="id" type="text" name="id" value="${editedSensor.id}" readonly/> 
</div>
<div class="form-row">
<label for="IPv6Address">IPv6Address:</label><input id="IPv6Address" type="text" name="IPv6Address" value="${editedSensor.IPv6Address}"/> 
</div>
<div class="form-row">
<label for="port">Port:</label><input id="port" type="text" name="port" value="${editedSensor.port}"/> 
</div>
<div class="form-row">
<label for="type">Type:</label><input id="type" type="text" name="type" value="${editedSensor.type}"/>
</div>
<div class="form-row">
<label for="metricSystem">Metric System:</label><input id="metricSystem" type="text" name="metricSystem" value="${editedSensor.metricSystem}"/> 
</div>
<div class="form-row">
<label for="location">Location:</label><input id="location" type="text" name="location"  value="${editedSensor.location}"/>
</div>
<div class="form-row">
<label for="resourcePath">Resource Path:</label><input id="resourcePath" type="text" name="resourcePath" value="${editedSensor.resourcePath}"/>
</div>

<div class="class-saveButton">
<input id="saveButton" type="Submit" value="Save"/>
</div>
${savedMessage}
</form>
</div>

 <br>


</div>
</body>
</html>