<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
<%@page isELIgnored="false" %>



<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" >
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>View Sensor</title>
<link rel="stylesheet" href="/webjars/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="/css/main.css" rel="stylesheet">
        <link href="/css/displaySensorsPageStyle.css" rel="stylesheet">
    
    <script src="/webjars/jquery/3.1.0/jquery.min.js"></script>
    <script src="/webjars/sockjs-client/1.0.2/sockjs.min.js"></script>
    <script src="/webjars/stomp-websocket/2.3.3/stomp.min.js"></script>
    <script src="/webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="/js/app.js"></script>
   
    
</head>
<body>
<div id="main-content" class="container">
<ul>
	 
  <li><a href="/addSensor">Add new Sensor</a>  </li>
    <li><a href="/displaySensors">Display Sensors</a>  </li>
  
 <li> <a href="/monitorSensors">Monitor Sensors</a></li>
 <li> <a href="/">Index</a> </li>
  
</ul>
<hr/>
<h1>~ Sensor Information ~</h1>
<div class="div-tableAllSensors">
<table>
<thead>
<tr>
<th>Id </th>
<th>IPv6Address </th>
<th>Port </th>

<th>Type</th>
<th>Metric_System  </th>
<th> Location</th>
<th>Resource_Path </th>
</tr>
</thead>
<tbody>


<tr>

<td> ${viewedSensor.id}</td>
<td> ${viewedSensor.IPv6Address}</td>
<td> ${viewedSensor.port}</td>
<td> ${viewedSensor.type}</td>
<td> ${viewedSensor.metricSystem}</td>
<td> ${viewedSensor.location}</td>
<td> ${viewedSensor.resourcePath}</td>

<td> <a  href="editSensor?id=${viewedSensor.id}"><i>Edit</i></a></td>
<td> <a href="deleteSensor?id=${viewedSensor.id}"><i>Delete</i></a></td>

</tr>
</tbody>
</table>
</div>

<label for="statsFor">Show reults for the last: </label>
<select name="statsFor" id="monthStats">
  <option value="1"  ${monthsInterval==1?'selected':''}>one month</option>
  <option value="3" ${monthsInterval==3?'selected':''}>three months</option>
  <option value="6"  ${monthsInterval==6?'selected':''}>six months</option>
   <option value="12" ${monthsInterval==12?'selected':''}>one year</option>  
</select>

<!-- selected="selected" sto select gia na blepv poio einai epilegmeno -->

<button onclick="refreshStats()"> Go </button>

<script>
function refreshStats() {
	var sensorId=${viewedSensor.id}
	var monthstats=document.getElementById("monthStats");
	var selectedInterval=monthstats.options[monthstats.selectedIndex].value;
	console.log('sensorId:'+sensorId);
	console.log('monthstats'+selectedInterval);
	window.open("/viewSensor?id="+sensorId+"&statsFor="+selectedInterval, "_self");
	
}
</script>
 <script src="js/highcharts/highcharts.js"></script>
            <script src="js/highcharts/boost.js"></script>
            <script src="js/highcharts/exporting.js"></script>
            <div id="hc-container" style="position: relative; overflow: hidden; width: 800px; height: 400px; text-align: left; line-height: normal; z-index: 0; left: 0.5px; top: 0px;" dir="ltr" class="highcharts-container ">
                <svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="800" height="400" viewBox="0 0 800 400">
                    <desc>Created with Highcharts 6.1.4</desc>
                    <defs>
                        <clipPath id="highcharts-26caayt-1">
                            <rect x="0" y="0" width="729" height="269" fill="none"></rect>
                        </clipPath>
                        <clipPath id="highcharts-26caayt-2">
                            <rect fill="none" x="61" y="59" width="729" height="269"></rect>
                        </clipPath>
                    </defs>
                    <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="800" height="400" rx="0" ry="0"></rect>
                    <rect fill="none" class="highcharts-plot-background" x="61" y="59" width="729" height="269"></rect>
                    <g class="highcharts-grid highcharts-xaxis-grid " data-z-index="1">
                        <path fill="none" data-z-index="1" opacity="1" class="highcharts-grid-line" d="M 180.5 59 L 180.5 328"></path>
                        <path fill="none" data-z-index="1" opacity="1" class="highcharts-grid-line" d="M 306.5 59 L 306.5 328"></path>
                        <path fill="none" data-z-index="1" opacity="1" class="highcharts-grid-line" d="M 431.5 59 L 431.5 328"></path>
                        <path fill="none" data-z-index="1" opacity="1" class="highcharts-grid-line" d="M 556.5 59 L 556.5 328"></path>
                        <path fill="none" data-z-index="1" opacity="1" class="highcharts-grid-line" d="M 682.5 59 L 682.5 328"></path>
                    </g>
                    <g class="highcharts-grid highcharts-yaxis-grid " data-z-index="1">
                        <path fill="none" stroke="#e6e6e6" stroke-width="1" data-z-index="1" opacity="1" class="highcharts-grid-line" d="M 61 220.5 L 790 220.5"></path>
                        <path fill="none" stroke="#e6e6e6" stroke-width="1" data-z-index="1" opacity="1" class="highcharts-grid-line" d="M 61 328.5 L 790 328.5"></path>
                        <path fill="none" stroke="#e6e6e6" stroke-width="1" data-z-index="1" opacity="1" class="highcharts-grid-line" d="M 61 274.5 L 790 274.5"></path>
                        <path fill="none" stroke="#e6e6e6" stroke-width="1" data-z-index="1" opacity="1" class="highcharts-grid-line" d="M 61 167.5 L 790 167.5"></path>
                        <path fill="none" stroke="#e6e6e6" stroke-width="1" data-z-index="1" opacity="1" class="highcharts-grid-line" d="M 61 113.5 L 790 113.5"></path>
                        <path fill="none" stroke="#e6e6e6" stroke-width="1" data-z-index="1" opacity="1" class="highcharts-grid-line" d="M 61 58.5 L 790 58.5"></path>
                    </g>
                    <rect fill="none" class="highcharts-plot-border" data-z-index="1" x="61" y="59" width="729" height="269"></rect>
                    <g class="highcharts-axis highcharts-xaxis " data-z-index="2">
                        <path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 180.5 328 L 180.5 338" opacity="1"></path>
                        <path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 306.5 328 L 306.5 338" opacity="1"></path>
                        <path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 431.5 328 L 431.5 338" opacity="1"></path>
                        <path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 556.5 328 L 556.5 338" opacity="1"></path>
                        <path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 682.5 328 L 682.5 338" opacity="1"></path>
                        <path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" data-z-index="7" d="M 61 328.5 L 790 328.5"></path>
                    </g>
                    <g class="highcharts-axis highcharts-yaxis " data-z-index="2">
                        <text x="24" data-z-index="7" text-anchor="middle" transform="translate(0,0) rotate(270 24 193.5)" class="highcharts-axis-title" style="color:#666666;fill:#666666;" y="193.5">
                            <tspan>Values</tspan>
                        </text>
                        <path fill="none" class="highcharts-axis-line" data-z-index="7" d="M 61 59 L 61 328"></path>
                    </g>
                    <g class="highcharts-series-group" data-z-index="3">
                        <image preserveAspectRatio="none" x="0" y="0" width="800" height="400" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAyAAAAGQCAYAAABWJQQ0AAAgAElEQVR4nO3dXZLjuNU2Wg3cF55TR/nWA3LP4T0Xp6UPBPcGwX9QWiuC4aqURILABognM7v8egEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAc619//e/fd7cBAAD4EQIIAABwGQEEAAC4jAACAABcRgABAAAuI4AAAACXEUAAAIDLCCAAAMBlBBAAAOAyAggAAHAZAQQAALiMAAIAAFxGAAEAAC4jgAAAAJcRQAAAgMsIIAAAwGUEELZSOwAArGYTyVZqBwCA1Wwi2UrtAACwmk0kW6kdAABWs4lkK7UDAMBqNpFspXYAAFjNJpKt1A4AAKvZRLKV2gEAYDWbSLZSOwAArGYTyVZqBwCA1Wwi2UrtAACwmk0kW6kdAABWs4lkK7UDMI5//fn7/+5uA0AXm0i2UjsA4xBAgMewiWQrtQMwDgEEeAybSLZSOwDjEECAx7CJZCu1AzAOAQR4DJtItlI7AOMQQIDHsIlkK7UDMA4BBHgMm0i2UjsA4xBAgMewiWQrtQMwDgEEeAybSLZSOwDjEECAx7CJZCu1AzAOAQR4DJtItlI7AOMQQIDHsIlkK7XDFuoGziGAAI9hM8BWaoct1A2cQwABHsNmgK3UDluoGziHAAI8hs0AW6kdtlA3bKV22gQQLvev//z937vbwDNZ0NlK7bCFumErtdMmgHA5AYStLOhspXbYQt2wldppE0C4nADCVhZ0tlI7bKFu2ErttAkgXE4AYSsLOlupHbZQN202kTm106Z2uJwAkjMh2yzobKV22ELdtHlm5dROm9rhcgJIzoRss6CzldphC3XT5pmVUzttaofLCSA5E7LNgs5Waoct1E2bZ1ZO7bSpHS4ngORMyDYLOlupHbZQN22eWTm106Z2uJwAkjMh2yzobKV22ELdtHlm5dROm9rhcgJIzoRss6CzldphC3XT5pmVUzttaofLCSA5E7LNgs5Waoct1E2bZ1ZO7bSpHS4ngORMyDYLOlupHbZQN22eWTm106Z2uJwAkjMh2yzobKV22ELdtHlm5dROm9rhcgJIzoRss6CzldphC3XT5pmVUzttaofLCSA5E7LNgs5Waoct1E2bZ1ZO7bSpHS4ngORMyDYLOlupHbZQN22eWTm106Z2uJwAkjMh2yzobKV22ELdtHlm5dROm9rhcgJIzoRss6CzldphC3XT5pmVUzttaofLCSA5E7LNgs5Waoct1E2bZ1ZO7bSpHS4ngORMyDYLek7ttKkdtlA3bdadnNppUztcTgDJmZBtFvSc2mlTO2yhbtqsOzm106Z2uJwAkjMh2yzoObXTpnbYQt20WXdyaqdN7XA5ASRnQrZZ0HNqp03tsIW6abPu5NROm9rhcgJIzoRss6Dn1E6b2mELddNm3cmpnTa1w+UEkJwJ2WZBz6mdNrXDFuqmzbqTUzttaofLCSA5E7LNgp5TO21qhy3UTZt1J6d22tQOlxNAciZkmwU9p3ba1A5bqJs2605O7bSpHS4ngORMyDYLek7ttKmdnNrJqZs2tZNTO21qh8sJIDkTss2CnlM7bWonp3Zy6qZN7eTUTpva4XICSM6EbLOg59ROm9rJqZ2cumlTOzm106Z2uJwAkjMh2yzoObXTpnZyaienbtrUTk7ttKkdLieA5EzINgt6Tu20qZ2c2smpmza1k1M7bWqHywkgOROyzYKeUzttaiendnLqpk3t5NROm9rhcgJIzoRss6Dn1E6b2smpnZy6aVM7ObXTpna4nACSMyHbLOg5tdOmdnJqJ6du2tROTu20qR0uJ4DkTMg2C3pO7bSpnZzayambNrWTUzttaofLCSA5E7LNgp5TO21qJ6d2cuqmTe3k1E6b2uFyAkjOhGyzoOfUTpvayamdnLppUzs5tdOmdricAJIzIdss6Dm106Z2cmonp27a1E5O7bSpHS4ngORMyDYLek7ttKmdnNrJqZs2tZNTO21qh8sJIDkTss2CnlM7bWonp3Zy6qZN7eTUTpva4XICSM6EbLOg59ROm9rJqZ2cumlTOzm106Z2uJwAkjMh2yzoObXTpnZyaienbtrUTk7ttKkdLieA5EzINgt6Tu20qZ2c2smpmza1k1M7bWqHywkgOROyzYKeUzttaiendnLqpk3t5NROm9rhcgJIzoRss6Dn1E6b2smpnZy6aVM7ObXTpna4nACSMyHblhb0X64ttdNmM5BTOzl106Z2cmqnTe1wuV/eJC4xIdsEkNyTamfrOO25R5uB3JNq52rqpk3t5NROm9rhcr+8SVxiQra1FvR//efv//5ybY1UO0vjIICMZaTaGY26aVM7ObXTpna43C9vEpeYkG0CSG6k2mmNQ884Za8LIOcYqXZGo27a1E5O7bR9W+287+eX9yHDMzi5b5uQRxNAciPVjgDyLCPVzmjUTZvayamdtm+rHQHkAQxO7tsm5NHWBpBfqrU7aicNCv98PXr9X3/+/j8BZCzWnZy6aVM7ObXT9sTaabVZAHkAg5N74oS8kgCS+6oAktzLtwSQkdryell3WkYbqy3OXBfvqp0nrO3fUDtrrRmXJ647AsjDGZzcEyfklQSQ3LcHkH/95+//PjmAlPc8Wl1ad3J3180RsnXxiHF/egA5cy5+Q+2staYeRl93sudX+v53AHn/7w+O//BGeviONgFGa89osgn9Dh8CyMXXbASQcDz+/P1/TwsgZ2x0RqtL607uGzYR3xZAsvVl67mOaFN47i+onTXWrtGjrzsCyBca5eH73gzd3Y7S2vZ8W4Ev/v98/PW/f7/fU2/oosVvlFq7wqgBZDJOAsih5zzK3tr5tnWo9A33dkcAObPfegJIz0bxfa6j2/c598NrZ23ffGMAqedKWu//3Hv5jHv6+H+lUR6+SwHklg3dig10thm/0952HBlAfu1fxXpaAFm7ebgtgBy0Sbtq07PFEQGk9dPJPee+2zdsIloBZPfYNzZke867dE0B5HytX6Wt3/P+swDS94zjJqMMSmvxzSZS60F7VJvK61wRQI68n9MDyD/fVaj75jPhTwwgPYvlnQuqALLQ1hsCSPnQmtXmQJuTswLINzyErxqnUzfCCwHkiBpfuuaRBJBrCCACyNcZZVCeFECW2vI1AWTpV3MEkKGundXnmgASblyjel+5UZq16aYAEm3yBJD5+/Zc/0xfEUCSn6zvDSCtDeeZNT5bQ6KfgL+fpUG/XhlARt9kt2QBJOu/Xwkg6VongIxvlEFp/T76rwWQo+7psgBSjdsdAWTpwXbYdTvvYYQAMtsQ/EAAWfpOqwDSF0BGeS7Urgwge3/Kl567ngvVT+Z6f1owe23QAFLekwCynQAigHydUQYlCiDlhj5awH4pgGw5Z6u9i59NwmB9/s9Ejza2FweQut8EkM4AUozfZQFk63d5Lwogd66LAkhOAPn+AHJaOzsDyFWBaK1fCSDNGg5+WtgVQDr2M9xglEFpBpByASsnW/Wg7fmu+No2ldfZGkDWtCPdPPQsnMl1vz2AfNoxaAA5ayMzu2YWQIoNwqRd9XeIVtReNL7Ntl0QQHo2b0cEkLPHUgDJnf2vOZV/PmPeRuf85gBS39NiAEk2lIe0c2cAyebEVXNFAMkDSF3fs/AhgIxplEGJflQmgMzb0tPe+rq9m8XyISGAJNc9OIAs/eMGa9tW/7SwHJ96rHoCSLYhelIACb8bVs7tatOzZxN4BAEkd0cAOXK8o/NFAWRpja7bvBSYyvVxd30FG+DJunJgAKmfK7va/UMBJKuHO7+xkl6395nTCiDJGi6ADG6UQdkUQILUOzln72Yx+ycr601KY5EvQ8pZAWTNJq8OTHcFkHpBbJ1vjTsCSNYniw/VpY3BkQGkethPNgL163UASfqwFUDqjUVXWNnxu9hLm7PNASSo32YbTnxYHxVAonXoCQGk2fc7A0jr81cGkGhjnW2Wep5xTwog0T9aMjmXABK3K+i716t6bg8YQBb3ER3PnNneUAD5DqMMytABpN6knRlAgk3gpw1rAkgwWZfa88QAUo9Lqx6OuOZS2Ii+1noQXBpAWkey2VoTQMoHQuscRwWQ+ieOowWQPfe45XOfz+8IIPUavPraK9veO58+rx0QQLJvIpwRQJbWxffXm6/3BpBGWwWQcwPIGT+Zi8Z6MYAEa3/5vrXfvDnC3gBSfwO4fI8A8nB3DEq6wV4IINFGOlqcy4Vwa3sm51sbQDYGoeieyrasCiDlA+HbA0i5gb4ogKQP0KLPJ5+7OIDMgtmKo2x/vVmv7yk8qo1OOG9bm8yF79JdGUCyDfKa+bTWNwWQnjVkzTlOCyB1DRwYQMJNUxUcstffbS7bWZ+/PM9SAFmae9lny7ZsDSCfr1dBoLmGHxlAOu99dQCp2nyUrQGk/Ebo0wNIWS8CyBe6JYA0HuqfiVN9x2ZVAGlsHrL2tP67i7sCSO/iMWtvtanq2TDNJn3P9ZKHdPqgPXijPbtWsAgtnmvFolvfT3SOngAyWUgHDCDRZz73kczJaAzqMarb2bqP9LVyDE4OINnY9Myn8v7X2r3RKjYhk6+fHEC2rH1hGxv9uzuAtMZ0wADS+985jhJA6s1vuB51BpDo14j3rJW9Y5nWXlCrn/OeGEAmz+XimL2vDCBFf5bvOzOApPuihfNm9fR+7XM/wZ5mcr8rAogwMohvCSD1Ql6/vtSerwogyWRttWc26VuLVbV5np1/4UHbakePZgCp2rF4rhWLbn0/0Tk2BZCdD9bJeaMHfrQYt45s81BuDlqfPzCAhA+ncnMWbPA+763WhqhOmt9de2AAmWxCTgggzXELNjFPCCCztSrYRO+6XmMz9KQAkv5krV4j9gaQaiNdv2dN26N29rzvc78jBZCyDx8YQJb2E1FbtgSQ1pyrv2nFzc4ciJ6Cyx76n8Iriy/ayCRFV7++1M5vCSCTftgQQOoHQdbGT1tvDCDZUbZj8VwrFt26T6Nz1A+Nz+fquj04gJS1F9XrqqP1mYMDSFiLyUN/VlPlRrGxcf22ANJa00YMIK1rjhZA6v+Wb+/G7PQAEszZtB3B86mrJsq2BM+VrnWjfH1PANkzFp2fn1xzIYCUbc3et6e9s/ZU9RG+3hlAorV1cv2Vcy3q294AkrXl0ABSvWfNvXGSJwaQyeK9FEA6Qki9gJRfD9sWPWSLh/5s4VjxAI3O8/n6AAFk1v97AsjKh0nXAlO1Y/L5oBbWtKHu0+i+6rH/fC556O8NINmGqa7XVceBASQbl9ZDqTuAJPc36ecomJXXrwJI1H+z9nXMp+h9vVo1/G5z+tlyE7c1gDTqsXU/6RisCSDJOT5/Tu69d/4ME0CC9WPpGfd+T9r2Rlubc+TqAFLef+t9FweQ2dpezYOzA0j2bGrVR9nOnjp7UgCZzMl6XiT1MrnXhWfTmnvjJNGk6v7swiAuBZDJRqx66F8eQJKFfda2bBF/L5hLAWTFBqDeiJQPgvSzRwaQamGdteOgAFJ/ly/tmw0BZDZOFwSQ7Dt80d+HDCALR3kPS4v80rhEc2NvAKnXhysCSPYNjC0Puqy2on6azalWAOmos7oPs7Y1P1uvvwsBJPvd7fp+Xy8BJFpD6zU7bEc2F8tn8ELfNudK/dyIjo0BJGv76rFY+Hx5X5/3V2tR9PdyzSv7a3X7Tgogk9fXBJCV9xGuVVWIDD8ngPyuowJI+KDuCSD15Do6gFSLw6wtxfnre2sukvUi/l4wdwSQOjxMrlHe94oAEgaEaNOXPShGCiD1uLYW2+S+szb0qBfA6JzlgzPqj/Lv0b2E12314Y8GkMmxsD50BZBWHUVzoK6roIZbY9qS1VbUT7N6qAJItFF533fdv3Uft9qWvTar7WTDP2nvwne6jwogzX55v3ZhAImeF7Martbget5Ea3ZPGyavHRVA1h7ZORbOvXksFj4/SgCZPWOiPhkggPR8M28pgDTnYzkno3mR1L4A8iBbA8hs47UQQKKH6p4AUm8O1gSQSbuPCCCNB8qsH74hgCQTfdYfZZt3BpCob5qLbbQoHRFAgrGd3XsjgCwtltm9p304YgCJaiq63+CBXv7v7M/RfSUBZLKGXBhAsvYulNZ0vJPa6qqH3gAS9XV970FIac2XcKx7AkhSV/X9vl4nB5Bi7kZzZ+n8WbvS+loIIOHmtl5/knWl7JOtAaQcx/frZ64r0b1k83bTWASfL589awJIc4wODCBpn9TvuSOALM3rOjwka9nhAaTVDwsBZO2vnbHTYQEkKq7g9e4A8l6gzw4gyeToLejJ640A0vMw7w4gyRhlC1X2YCr/3Frs0n6J+jwYq/q9s5qoNuzhva0NIFEtVLUXjUGzDeU4B+P9ec9ZASQajzJ8XBRAevo/qoXofh8RQJI5MLtOtqHcEECya9f9tLiGtDYu1cN/du/Jhmh2veAnXp/XBJB8rhwZQBr9ltZ+Pf7Br8JNztOaKxcea/p+8veofk8KIGvrpW5H2d5snZu854wAsnAPZwSQtN3R+hW8JzxHNeda93ZEkGSFoQNI+UCIFudGAMkWh6hddSGXX5+9p7UIXBBAogf6rP+WJufAASTrn1UBJBqjYOMfXaPu17CP9wSQajHsCiBBTX5e6/hvGNb01SFH62HSGUCy+TI7z44A0tU/awJI9f5Zf3R+d61u2+z1KwLIP/U5uV5Uf0kAmfTNAQHkc42lXxNK5vCkXh4YQOqNU/iZoN9a74nGv+6TyXkeEECi+4/uaTI+wR7i8/7g9clrNwaQTc/EnQFksoY8MYAEtV63J/ozJ5ktxk8KIMXE73lP1pa6kMvzheeuF/KyrUsBpNFnWwJIz4SNFtXymrPzRMEi2IQ0+6Ke8N8UQJLxrt+Tfi74fLZAT8Y+eE+98V2shyuODQFkViutALJ0f0cGkKjPs+sU7anf9x6rVn3Nxjsb8xsCSKstTw4g4ZxM6io6b8tiffUGkKSPw/b/ma9n2Xt+PYCUc3dSI2XftvYr1RhNzhv0X7Pdxa9vXh1Awmdz8lsJkzVkaV6X62IydvU60bzH8tqN2ui5/6yOsv0HJ9kbQMpBj17//L2axK1fV1icTNEGOXrP1QEkaPvnOkcGkPrrCxM2WlTr8e65x8V+yRaQahM4G4eeALJ0naX2bwwgvRvg6D3Nz1VzoDmXgnH8vLYUQO44rgwgjTaMGEDqc0WitkU1uTWArNmMf86ZtaV8X9a/rY1KUPuz+XNHAFmogR5r66tZmysDSHMe/inWxAcGkNYepa6b+rVZv9wYQCZtvTCAzK5Tnacc66jN6XmivukNICvuYdY3na/VYx+OiQByrSiALA3S533VoEevf/5eT9hsYzBYAFlqS3jt+vXGIp/16UgBJOqXxc/U1+8IINkDsXVP3cdCv5R98W7T7GsLfVO/p/m5pXqqr5u0d5hNQdUni3Myem/5QAw2lFvbMmIAqf83PH92/RMCSHjfjf++btLWjp9CR/c3+0xynU//Fr+mMuuzCwNI9Osy6TW/JICsuZfTj44AUm7o63uKAsjs/urnVvQN01a9Vv3Xamvd3u4Asub1qq5a4/ppT/X+ug8X29sTQKK+XjtXymPpeVi1NWpLND6c5IgAEg7mUgDJCu6MAFIsBNnfy/NOzndBAKk3FOGiVt3T5OutheiJAaQ4f2ucVh03BZC1ba3HKPsJx5ZzX3l01WZ5n+Wf6w1iT422xv0hASTdyNfXTwJIGjiye+7YWEZ19vlacc9L4x3139KYTu6r+I/8X4HuAFLcd3j93gDyp1q7q3tMx6N3vhwRQJbe0xtAVs6V049vDSBBTUVzKeuT3r67NIAsjF3Y12vnSnkIIM9yRQBJizILID2TqKcgo813tTBE75+1eetk+DNd6Os+nS04FwaQ8oHeu7hNrtf7nfeeABLUR90/q2tkZb9MxqFcqFYEkHTsV7Rzdv0fCyDRpig935ZxX/t6VSNhvXYEkPI9de1/ztsbQKrxL+t38f7OCCA941P+R7qdn5ncV7FRfAVWBZC67tbUdD2OAsj1R/H8mvX5u81rAkg0DzYGkPo8Ze1lbf20N6ipWXtafdLbd/W9nRFAGm2ajcnKANJ830kBJBo/DhA+xDYGkFmQ+VMtgD0BZEMBTq6RHPV70nuI2rzivM02vydH/S+aLASQqP8+/byi3z7vrxfdoxa3znOUbW/Wx8I47amZNQGkOc6t1zc8tCdj9A6JQfsP65ezjp7NcPa5KwNI7xFsoPcGkFn9nRFAlmry4gAy25w1PjO5ry8PIM2g+E//9T73Jl9vbciiuRZtoDvq5NLj5gCSjVk05pPaS34V/NPeoKZO6bs1AaTor7oPtwaQyX13vr/7EECe5ewA0iywHw4gk43KxgCya3Nbnu/GALIUSLfcZ9c9LAWQ6trpOJ8ZQN61ErT/sH456xBATgkg9UZl8tqWe31KAPnnHNmzadgAsrXeBgkgs2fFEXNp71EFkGjTWI5xOm+DfgjvO+n/NDRmASSo6Wxer63N1f0XtT+rqycFkI57L687q49k/1u/l4M8MoBsON73ky0M6XuPDCDBYvZuR9aeWVsWNuxdba0XoCsCSM+5zwwgjeuWNVv2dz0/WucJX98TQN7HrwaQ8t5b/b+lHVuOkwPI+/XsPsv3fPrmVwJI1d7o2VS2K3xeZfe2pa+CEFi3Z/f8vDiA9HzneJj1Jggg5X2Xz7mwBi4MIFEgmbW5EWpP67+o/VldnRBAsutcUTvldaOaEUAutDmANAY3fX1lgZ4y6ZKFoTkx1kyi1vWzxaw3gNT3cVUAuWpc7g4gVX/X86N1nvD1EwPI7eO2d6x73jfav/BVP2jLDUc1l8L3/XIACc619JnwWRJtDqp19f21nufVnr6K7mdyzScFkKW2PiCAzMa1mFPhfGzd35YAkp2nJ4DUNXVmX18VQHrb0erPE+599oz/U+2NBJDrhBMjCSCLAeNJAWTNBDziHlqLypoJ/M0BJGvzBQEk/fNSADmqPrZ8fpQNwdF98aQA8v7f1lzqDSDBa9F76r5ZteauPKL59/nalgDS+Hv0/tl7WgEke/3gGmjdT/28POI6n/HuDCCzzy1d56kBJOuD6JsBjVAQfk0Amby/ORe/IIDUa3n0nnp+c4DJ4AT/0sPkvcV3FFqDm76+VIAnF17ve7vbv+a8SxvJLefZGkDu6P+t/XfWYtwRQFqbjcPrY2vd3D1+Z/bFjwSQ8vOttTULIM3P7TzC81b303v9+j1LnwmfJUsB5M/82XV0DYR/jq57cgA5bP4/MYC0aqgngCzdXxRAtozbQgAJ7+dXA8gI9RME8mgvzEGywXm/NnlvZwAZZqHaWOzp/e0574gBZISj5x4GCCCn3t/V9z7yWJfHrwSQ8nPZPY8UQN7tOTmA9Jwnet7Uz64z66F5XQHk9H5vjm8W6m8MIOFmvZ7XTwkgf3YGphEDSOM9G7bXLOmZtJ/3CiDbzyuAXDJ2W85Z1/qp13X0H08KIPXXgs82/3uepXteCCBnHa21fpgAErXtz70BZPcmUgDp6pvm+B4RQMr+3zJuXx5AdrV3gACypsYO2XAz1TVpyx9L9WykR1moNhZ72C97z3vkw2LjOVfd10jHhQGk/vfPb7/3Xz5GDyDF18M/1+9ZePA373m0AFK3oWOuzM61dX4tbUBHCCB77i/rawFkuaaSOljst6V72zrfon5b+/ezDgGk6z3lvOZgWcfPXhNA9p335E30bW0YbOy2HJPxFkDGOQYMIItf7wkgW+75pgDSHZauDCCtcxbnTZ9xJx+nXPPsANLzngHXw2Zf9wT+o/uyp98EkGvvc8t1g/dcsyP/MdkAzF7rDSB3FNTBxd7qlyuuf+s5f/BIx1v/5scVffOUANLznr39ddcG8MBvsITPlJ3tC+du9k20i47LriuAHBdAzhyjUQJH57hH8zTdL0bBePRDABlHNgCz1wSQy65/6zkdjp7jitobrb4FkF3vO2NdDc8pgBx/ntHmYk9fPyWADHaEe7+srwUQ9lhVhE8OIHcfAojjmw4BZN17jrgXAaTvnALI8ecZbS6+j6X/o8W72/ftAeQB97OpvQLINVZPJAHkvKIf4ZwOR88hgFx/L6P1x8q2rXreHHBcfb3PMcJ/n3P0eUatvSfsR54eQBa+Pvr9bGqvAHKNVYMmgJxb9COc0+HoOX6x9gSQXW1b9bw54Lj6ep9DALnueMJ+5GEBpPu/DfnmQwC5xqoBGfQ/RHvEIYA4vun4xdoTQPa1beT2O545dqO2q9XG0dssgAggVzmiMB039Z3xcNx1/GLtCSD72jZy+x3PHLtR29Vq4+htFkAEkKscUZiOm/rOeDjuOn6x9u6+57uv/+vt/+Vj1LEbtV2tNo7eZgEkPO7eq3+lIwrToe8cP3aoZ33+a+3/5WPUsRu1Xa02jt7m0dt303H3Xv0rKcyLDn3n+KZDPevzX2v/Lx/G7ri+G70vR2/fTcfde/WvdPeg/sxhUju+6VDP+vzX2u9w/MJhnobH3Xv1r3T3oP7M8WuT+tfu99uOpfEzvuONicPhcOw9rDPhcfde/SvdPaiOLzz8c83b+uzuNqxpz2jtdTgcDsf+w9oeHnfv1b/S3YPq+MJDANnWZ3e3YU17Rmuvw+FwOPYf1sJqEmEAABjjSURBVPbwuHuv/pXuHlTHgccoC4cA8sixm6wH7/Zk7RqgvQ6HY5DDeuD48uPuvfpXuntQHQceozwEBJBHjt1kPRBAHA5H72E9cIx6HFSbd+/Vv9LtxfG0Y+SFdpS2CSCPHLvJeiCAPPswPuMe3zg233hPTzyMw7wfBJBx3V4oTztGnuCjtE0AGX/sgutN1oMygERte/r4XtX+u/rp6ePzzcc3js033tMTD+Mw7wcBZFy3F8rTjpEn+CBte71er1HaMkw7RmunAHJN+0cKIE8fs285vmEcnvZ/rvcrh3GY94MAMq7bC+Vpx8gTfJBNx+slgIzWzs9cb/xq1WQ9EECedZ2e6z59zL7l+IZxOCuAjP4rn6O0Izns6Ypx+ut//z5yzO7bpX+x2wvlScfov1pUtO0ztle0t7rG6yWAjNbOWT38YgDp+a7Y0XNcAHF84ziU9/DX//59SL399b9/p3uSUfpslHYkR7iGP6Ddp4yTADK+2wvliEK78lojT+QRAsh//v7v6/VPABnhu8wjj9dF7ZzM9TqARDVTfn3P5mK0vhdA+toy2rjdeZzRF3f27xk/qfhnjZjtJ3rnW3GedE8ySk2O0o7kCNfwB7R713hka50AMr7bC+iIArxqggkgi9cdKoCMPl4993DAMZnvRwaQ9wJ/wz3t7mMBZLw2j1IbZ/bF3WvSlmsv1dJVAWSEuhyhDY0jXMMf0O5d4yGAPNftBXREAV41we5cBFd+x/k9tqeOcdT3Ash547vxmMz3csyqB/6kVuoAUrfvrgCy55wCSF9bnjJnzu6vM/oi26zffZ9rPyOADHmEa/gD2r1rPKJ7++t//34/n46aa+fvxn/Q7YW59/oXBpBPf93RZwLI+r4Z5aHV096j2hmEgnC+/zNOZX18/lz22zcEkLpOs5/qlPd+VO0IIM84Lgwg9bwb4j7XfiZYI2b3JICccrRqZ/KaACKAjO72wow2Nls+P0oAOWtzeXYAWfsALvvhzgDSs5k8uw1HjOVRNVKHgiJoTOZ7GUD++dqnVsoN+N0BZO/6UH5GAOlry5Pv++j2Xh1AjvqGXOv1rc/Y4u+TtSS7p3oDvFSTCwGk+1l2dv+d3YaFo7Vvm/RR1bd3t/uMsUr7oqilo+rmqD03hdsnVO8GY+nrVweQVnuCTeDh/bTwntermHwrP9v99TsCSH3ObINct3GkI6r5XwggR3z3dW34L77+ub8qgHy+Xt/7kzfiAsgx7b06gOzdKH5jAPnns2mfXVEHZ9XDhqP1TJ+tZWVdDdD2o8cq7Ys6gBxw70fst6ncXpT1JvbhAeT1er2GDCBbv1u9MYCcVltBAEmv9csBJAgak37qCCDvWg77t66png3vaAGk+Ppsbv9wAPn00133fUefjRBAetbpvX13QgCZrC/Z51oB5H3PvxpAVp6z9WxNx+DqAHLFtXoCSOs9K49VG2v63L5BKydIq3CXJtwFm813f7X67fW6P4B8xrZu89Jnq2s062OwABLeX9bGs9u09v1HPVyrh3n54J6MSfX1NQFkco41AWTr/BRAjrvuTQFk9fmu7rOlX8k7+FrN9WrvRvGM5+RBAaR5z3+mz63J6wMFkFPacFYAKce6559TP7LWt55rxeda+4xqmyuAjGqoALLh3wH/vP+Czea7v1r99nrtCyBr7/9zVD9yLP+8OoDUm82O93/uWwBZ/f60/Vvqc0MAmdRKXTNV/07Oc0EAmfVL6zx1m6rP1PfX6hsB5Ljrrz7fFXO2PI4MIJ3r9KRPyvtdCiAd52+O6VUBpL7OUgBp/RrXWQFkYY5E1+tqw9E1E7UraXs6Bq2fLm2tiyP7IPpcRxgM+6Ja1498vnKwkQLIp00rCvrz/pM3m2V/tfrt9doeQLbc/+d4QgBZ+ntnH0X3Hd7fFQGkd+FuLKzNcV/bN6MFkPInm78WQI5+qK+9ds/Xevp85/VXny9ah87sqz3/Xzdr+7gjgDSfAQ8KIOXfW2v0qxLe868FkMazsuz7up9mdSWAxHW14YjOyU67iu6Igu0MIItfF0D+/f5seZ5Jm5fGYWUAmV3vygCS1M3kPgcOIGU7m+Ne31PjtUntJQGkruOo76L3z8a6vIfWP8N5RQCpNnDNh3P5K2UdAWRx3hxRG2fVZc/XVtbbluuvPl/wK3Kn9s/ZAaS61my+CyCzdWZ2zwvP3d3jEox51N5mLW591mwNIPVm/RcCSNUPYU0IIM9xagBZszE7KYBkn1s8gn8p591fUb+9r/N6jRdAmuctzy+AbGuHANIOIOWmfms/l39fGUAm7X3Pk6pWZ/d1ZAA5oO739tuaWt11z8E9rj1f2f+HtafRvrMDyKT9jQAyqc9WPbWuX87/4vqfcx4YQKL1on5Pfc/RGhGtM7N7vimAhOtPdo6jA0jyE+Z6fkRjMum71q+3jxhAWs8LAeS7XB5Ako17dwCpHgifr395AFm8j18MIMGDbXKfIwWQv6b/x0hlO5fGp/e1yXlWBpCsZrLXyg19HUDqMTo6gMz6oyOA9PbBqgCydE97A8iOmg3b3dmW5j33Htka3BuA1gSQLf0UbeiC96zuiw0BJJp3vfU2e28jgEzqe8M9lfM6WhPqcS7Xh+w9hwSQnvE/I4DUa8RRAaQMDq16fX8t+GZKNgbp/e5Yo7v7u/dz1U9AZ/dd1XXUN3VNra754IjOyU6PCiD1pPxTbXaCDUh2vsVjhACy5j6qf3aubM/iWNeLT0cAia7xuAByxAbvxwNIPe7RGI0aQFr9ckcASe9rT30utXfUAFKM42J7Dggg0TWyr69ty+QcGwLIZD41avt9/sn/1u+5KIB8/r4QQMI5GNXsEQGksan/9NnKADJ5z2ABJBqD6NqT8w0QQMpajWp99vetAWRlG6NzstNhAaRzYXi9gu+SVJvJbPCbAePEAFL31+s1Dxnv60xeK3//fUWBp4vOQnvL9q0a64sCyOxrWzekxViH91nf14r6XKyJrO46PluPTzru2dgvzKfJeQYNILP39Ix3a46U/VJfr/pMj8cHkGxTVLy2NAd7x2apHeH5OuZhWVNd7dmycbojgASbxPL16LX689Fzp6zd7gCy5tlUPQePCCD1ebJ7PzuATK73oAAy6YvBA8isf7L+jOq5MR+j93XX1YZxis7JTqMGkMn7isKKgkH0enae7vvpDSDVQ7Js0+EBpHWeFQFk1ifl4lMsQq3P1H3/6ZPkIRvew84Akt1nVCvpIr/m+jsCSF0/Zd9M+q819sl8ympvqX/Cr//JF9r0Pq4KIK1f5YiuV/VRj64AUrwnbEe9hhwcQNK+q+bF7BzfGEA2bJzqPpjVUzBne+81+m9GyrGI6recE1Fb6s+3nk2XB5DGPX2ukwSQTDi+xfNtqX6adRIEkLJN9R4iHYtoPPcGkPJ/G22dXW9tAMnWhCcFkKoG67ndXVsr7ik7Jzs0B2HNwhs89HsDyKxNSWEKIAcEkGhTVYWPWZ82ForZQrcigKwam/ocawJItgk8KICEY5YtnsmvYExqKhv73gCyED7e5wu/3lhoJ9dYGUDKz3TVc1QjSQCpr9d7P+E9Vm2dXL9at0YMILP+KTYp5T22rpGef6Fd2dxq9lFrrjwtgNSbxvrcVweQxvPr8/4TAsjk7wMFkLI99XhM6mJjAPm8tqJPZ/VT3XPzWo3n4GS8egJItlZtPc4OIMF6Vv496pP6WgLIAMJB6N043xBAyv+t31/eU3aepQKb9MEDA8jiWBd9OBu7DQFkcv5GAJm0IVl8usdoxABSXi8Y87CNdd/0/Mg9mGfleL3P0+qb97myNi19tv589tPLzz2dFEDq631lAGl9Rz2o5/reowAya3O0IamvsxBaWnOr2UeNufLEAFKfZ3LuhQAdjs+fYK05O4Akz53sHHW7J38/IIC8z1O2pWc+Zec5KoCkbT8ogEyunQSQVh9GYxCNb3m+WwNIFuiiuXF1AFnYb7CRAJIs/MkC/nnfeyHbEkCSiT5rxwkBpPzzrD3RJqa8v2ThmvRJTwDp2Vg0xqo3gEyud1cAabR11jcdASQbh2xMlvonatPaz28NIO8/L453sanK2nvUQ6L1EKzXrTSAFJv+WwNIee2VASR8X1KbWVvC91TzIuqPSfuX1oneANJYS8NxztbKhTbU55mc+wcDSGTpPeFcWwogwTzJzjNkAClrp/x7MUdm17orgKx8T6tfJv17ZQAJ1r2e+uFgdwWQ9+vNttWfywJI8vdZgSdHeM2TAsinX3YEkPCeTgogk37cGEDqNixuLBp1V56j514PDyDJYvctAeSoz3/G6oAA8nnfUgDpmANb7it7GE0e6n+CB+0PB5C0X0YKIH/l/7Fx10Y9aEN9nsm5Dwogdd9MvvalAaS+7+Y8Sd7zen1XANnSx9H4Tvo2a/uazXq2RgggRGadPVIAKR9C2UauJ4A0ii3si6MCSLkp/TN/uDbbcXUACQLEpB+rnwAsXSN7fVUACcZqbQCZ3W95z52bw6wmwgASbLTCNtY1IYDkm9Gkxid1NEIAqTb9a2pscq1qTqbvq95f30vZlvCeFmp1aU6G76k24p8/l3OjZ9Pes05k8zipq8m91QFky8asakd2L2W91H37/nNWc2X/RX0zu6dqnajvZfG+Gs+d+hyz63esQ0vvieZa+npQb9k9ffqnGveyTdFzMJt7adtXbtBn1yprqZpTvXrbN3mt9Wxes1nP1ojGnqfZ3mhuNNZ6AeRhZp29JoBUD69o4Yom8Pv1xXZVkzF8X/DgDc+TFFvUF2X7w2t2BpC6H8o2tgo8fRCUi215jocHkPLPWU1F/dB7z9l5JtddqvnRAkj14Orpj/Jaa96/1uSBX83f2TxprBmT910QQMJ7WXgYRbWZzf9WjU2uFW1CovcF7/+0q5zXSd/V60l678U9ZQHk/efJ9cv3lPdTtTcc884AUvdRz5i9XucHkOh6Ud/W/Rb1x2y+dwaQ+vX6vrK+iuZk/bnwnB3r0NJ7FsctGdvm/Cr7biGA1PeYzb207dkzutHH9efrum71V9aOVvuW6ry+767NeuP5lZ2jq71RHwkg36Pu7M/Xjg4g1QakJ4CU7+vdYOwOIB3FVv9eZfm/n/NUvxs/e9hVv7IT3nvwufCeTgwgk3te6J/DAki9kU/6YfGeF0JgeN2s3qvxnpwj2FzVD5NZ2+r2LASQSR/WD9UND6krTObJmQHkn/o95R6SOqhf/7Tj2wJIdU/hXArWwPrrXQEkWQejvsnWs6x/6nt7f252v/U9BD+FCp+DCwEk7NtGv83upyOAlPd0VgBZuqee9y2dozVu2dhO1tzG3LgsgLTq5uQAkmle+4gAkv3aefm16jrv19P2RvNx41o/m7NL97SwfnCA9AE3WABZcz/heZJ21u9dG0Cyti/9nmIzgCw8zGb3ckEAed937zWy15sBpA4f2Ya0N4yWm8BgDD7XzX5dJrp2dA4BJBQFkMnr9bqTjHd6/g01sUX9oI1e+/w9mJOzdbP+bzvqzeABAeT92dcr7pvZdRvvKQPeiAGkHofeDUR3AAn+W5ysPb21u9Rv0dpYtju7RncAqX6FcLb2VOO95n72WDpHNrb1upjNjVYA+fy9FUCKuRC2LQggs3avCSAHrmvltWev9QSQVqh6z5Pkp6STPhFAeNsTQCbvjQJItal6v//9em/7dt1ffU+NXyXqLbYjAkj59TSABIvl5F5WBJD6/NECvbuve/uuWuQ+f67C0NoN6eK1ozGoHrZlu6JrZ+MY3X+rP2bnFUAWg3f0+YObnF6nfNBGr33+fmUAKb/j2Agg6T11BpD6nNmcyDZ2k2tdGUCS92b909rwtJ6DuwNIRzvqdqebtpVreXg/Qf32nqvnfXuk+5A9AaSxbmdrVNq2KvTParKxvodz/+AAUv7v7PU6gBR9+llvtgSQOpR1htrWfNx6/7O9UzUe2Rzfcj06tALI58/B8fl8MKDv804+v2JztuV9i+cJHv6z93T8FOD9vsVr9QaQ6GF6cQBZ89ml8y6+pwogkzY1AshRbZy1p+i/vQFkdu6LAsjI1gSQ+uuL575gw1O2J5prs3FcCCCvVzDWjbXyrABSvt7za1pRe8r3vc9Tfz46z8gBZPaeCwJI9PXWOU4LIEkgvWqe9cg2h0MEkKQuZ+1Ofp25HrvbA0iyzwjnQG8AqX4ytNTe2VgdFEDS+03m+Jbr0eH0AJJtogcMIEddb7aAJxPukACyZ0IePLH2BJD356MAcmQbZ+0pFvm1AWTXdevzrgkgD1wQvzWA1O1YG0Den5m8rzOAfP68IQBP3rc2gNTfUCnuKzx/dE8rA8jkOlngqvrk9ABS3VN5L9H1svZG7dobQHpF34z6tQAyu8YVAST4Zufs7zue7Vn7ll5Pn8k7Akh9P6MGkNn7BJDzhQ+44Ds6uwJI9LC4M4AcuICn19oRQD6vnxxAjtbTjrIe0s1N4z1Hy35NaBIETujfPQHk6LZcobUJjB7aQ256ku9IHhpA6g3TjgDSfX8rAkjZpuj+w/Mnm5rJdY4MIFX46H7WZM+E7FdPBJDLHRJAOjbi0fXen+/5XPn5WbuzoB4EkFY7j5YGkPd61RlAyvfMrlF8c3GxPY35uOH20p9W19cSQC7WSvlLAeT9/uEDSLW5FUDuUffL5LWgZr41gLyvMWmHAPL/f+1LA8jn70cEkIX1ddX97Qwgi+ffGkCy7yZntbQzgKTtb2y86rYeUbtdAeTI52IQQCbvGXEuLgSQz3uj9eWkAJK1t27LkwLIux2TAFKH8WAf0poHdwWQ+pwCyCCaAST6zk9HAKlf39y2JwaQejK2JvaOALK3nXdoBZDyPXcEkMnXOx4Yu68tgLyi/v32AFJ/bU8AKT+/6x5XBJD3a93n7gkgHRvFpTWhDiDRtbbIfpqVBZA913qf66oA8nrNA2AW/EbQDCAb62p2jZMCyKednQHkDlsCyOc9RweQxv6g62aiz3YEkPc1BJCLRJPk89rNAeQo3QHkgM1mbwAp25We58sCSGnPInRYGxZ+b/3M6zc3pQPPpS2+OYBM3rshgJRfC89Xra1n12WPPePT+5ONVs202lQHkK3tzJwdQJbOc0YAeZ+3/N8RRd99/3z9zADyvs6JAWQEWwNIdJ4nB5D3175lrzW0vQGkft91Le/XG0AOu54AsugJAeTMa0dtWQwgAz+8tho9gLxexWZmSwAJ5r8AsnyuIwLIGa4IIM3rnxRAyvMfde6jXRFAJu/duQn9pgDy+XtR61sDSFdbzn72CyBjaH0HJAwgjQEZbePwVn9X7MqN7VIA6T3P52tfMimGDiA39G8dQLL3XNqoCyz9WtMIPuPS8w8tVAEkfE/wzYVfDSBHf/6OAFK+dtZ1P9f44QDyek37f7ZXCcLGnvsRQF6z/87sKwNIMpe/Za81tJ8JIOWPEB8cQHo+9wQj38OoAeQbCSDTa8zeN2AA2eNbAkj22lnX/Vzj7PsbvLYEkPOsDSBrzlN//m5LvwkjgFzkVwJI9Oerr73mta3nfIqR7+G2AFL8iztXX/8uTwogh51vRwB5vcaeO2cTQJ4dQPfK9iFPCSBb23KFNd8o3Vrro/SBADKI3gDyfq8Asv3aa17bes6nGPkeBBBKAsg47g4gr1djoyaAnO7KAPK53q8HkAO/STRKHywGkPe6O0h7v1brd0DXBpBRfVsA+Qa/fO+RUf/PvxBARiKACCC9AeT99d3X2/mNQgGk+NwgfbA0j5YCCgcRQO4zUluu9sv3vsTCN5YzanW2gWr8+oMA8v/8egB5vX57/F+veB9y1rgfuecZfdzW1O8vBJAr2/OzBJD7jNSWq/3yvfMsV9TqUgA5+/rf4hcCyK97agAZ3Zr7fHqtLwaQgf+xgJ/xHqRv+td5RlpMRmrL1X753qGXALKOAPL9wl9XPHHcPau+T/2vezGgTwCpfpcR9jL5oY91t58A8pt+/b+NYR0B5CEmv75kgQW4lHW3nwDymwQQ1vArVg9kgQW4lnV3ndMDSHJ+4wRwEgsswLWsuwD8NA9CgGtZdwH4aR6EAADAZQQQAADgMgIIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMf5/wChc5aSOZ4FHgAAAABJRU5ErkJggg==" class="highcharts-boost-canvas" clip-path="url(#highcharts-26caayt-2)" style="pointer-events:none;mixed-blend-mode:normal;opacity:1;"></image>
                        <g data-z-index="0.1" class="highcharts-series highcharts-series-0 highcharts-line-series highcharts-color-0 highcharts-series-hover" transform="translate(61,59) scale(1 1)" clip-path="url(#highcharts-26caayt-1)"></g>
                        <g data-z-index="1" class="highcharts-markers highcharts-series-0 highcharts-line-series highcharts-color-0 highcharts-tracker highcharts-series-hover" transform="translate(61,59) scale(1 1)" clip-path="none" visibility="true">
                            <path fill="#7cb5ec" visibility="visible" d="M 84 144 A 10 10 0 1 1 83.99999500000041 143.99000000166666 Z" class="highcharts-halo highcharts-color-0" data-z-index="-1" fill-opacity="0.25"></path>
                            <path fill="#7cb5ec" d="M 80 144 A 6 6 0 1 1 79.99999700000025 143.994000001 Z" stroke="#ffffff" stroke-width="1" visibility="visible"></path>
                        </g>
                    </g>
                    <g class="highcharts-exporting-group" data-z-index="3">
                        <g class="highcharts-button highcharts-contextbutton" stroke-linecap="round" transform="translate(766,10)">
                            <title>Chart context menu</title>
                            <rect fill="#ffffff" class=" highcharts-button-box" x="0.5" y="0.5" width="24" height="22" rx="2" ry="2" stroke="none" stroke-width="1"></rect>
                            <path fill="#666666" d="M 6 6.5 L 20 6.5 M 6 11.5 L 20 11.5 M 6 16.5 L 20 16.5" class="highcharts-button-symbol" data-z-index="1" stroke="#666666" stroke-width="3"></path>
                            <text x="0" data-z-index="1" style="font-weight:normal;color:#333333;cursor:pointer;fill:#333333;" y="12"></text>
                        </g>
                    </g>
                    <text x="400" text-anchor="middle" class="highcharts-title" data-z-index="4" style="color:#333333;font-size:18px;fill:#333333;" y="24">
                        <tspan>Highcharts drawing 500000 points</tspan>
                    </text>
                    <text x="400" text-anchor="middle" class="highcharts-subtitle" data-z-index="4" style="color:#666666;fill:#666666;" y="44">
                        <tspan>Using the Boost module</tspan>
                    </text>
                    <g class="highcharts-legend" data-z-index="7" transform="translate(330,360)">
                        <rect fill="none" class="highcharts-legend-box" rx="0" ry="0" x="0" y="0" width="141" height="25" visibility="visible"></rect>
                        <g data-z-index="1">
                            <g>
                                <g class="highcharts-legend-item highcharts-line-series highcharts-color-0 highcharts-series-0" data-z-index="1" transform="translate(8,3)">
                                    <path fill="none" d="M 0 11 L 16 11" class="highcharts-graph" stroke="#7cb5ec" stroke-width="0.5"></path>
                                    <path fill="#7cb5ec" d="M 12 11 A 4 4 0 1 1 11.999998000000167 10.996000000666664 Z" class="highcharts-point"></path>
                                    <text x="21" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start" data-z-index="2" y="15">
                                        <tspan>Hourly data points</tspan>
                                    </text>
                                </g>
                            </g>
                        </g>
                    </g>
                    <g class="highcharts-axis-labels highcharts-xaxis-labels " data-z-index="7">
                        <text x="181.40243827546" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="347" opacity="1">1970</text>
                        <text x="306.68777119907" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="347" opacity="1">1980</text>
                        <text x="432.00741007364" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="347" opacity="1">1990</text>
                        <text x="557.29274299725" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="347" opacity="1">2000</text>
                        <text x="682.61238187182" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="347" opacity="1">2010</text>
                    </g>
                    <g class="highcharts-axis-labels highcharts-yaxis-labels " data-z-index="7">
                        <text x="46" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="225" opacity="1">5</text>
                        <text x="46" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="333" opacity="1">-5</text>
                        <text x="46" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="279" opacity="1">0</text>
                        <text x="46" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="172" opacity="1">10</text>
                        <text x="46" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="118" opacity="1">15</text>
                        <text x="46" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="64" opacity="1">20</text>
                    </g>
                    <text x="790" class="highcharts-credits" text-anchor="end" data-z-index="8" style="cursor:pointer;color:#999999;font-size:9px;fill:#999999;" y="395">Highcharts.com</text>
                    <g class="highcharts-label highcharts-tooltip highcharts-color-0" style="pointer-events:none;white-space:nowrap;" data-z-index="8" transform="translate(58,144)" opacity="1" visibility="visible">
                        <path fill="none" class="highcharts-label-box highcharts-tooltip-box highcharts-shadow" d="M 3.5 0.5 L 152.5 0.5 C 155.5 0.5 155.5 0.5 155.5 3.5 L 155.5 40.5 C 155.5 43.5 155.5 43.5 152.5 43.5 L 82.5 43.5 76.5 49.5 70.5 43.5 3.5 43.5 C 0.5 43.5 0.5 43.5 0.5 40.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" stroke="#000000" stroke-opacity="0.049999999999999996" stroke-width="5" transform="translate(1, 1)"></path>
                        <path fill="none" class="highcharts-label-box highcharts-tooltip-box highcharts-shadow" d="M 3.5 0.5 L 152.5 0.5 C 155.5 0.5 155.5 0.5 155.5 3.5 L 155.5 40.5 C 155.5 43.5 155.5 43.5 152.5 43.5 L 82.5 43.5 76.5 49.5 70.5 43.5 3.5 43.5 C 0.5 43.5 0.5 43.5 0.5 40.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" stroke="#000000" stroke-opacity="0.09999999999999999" stroke-width="3" transform="translate(1, 1)"></path>
                        <path fill="none" class="highcharts-label-box highcharts-tooltip-box highcharts-shadow" d="M 3.5 0.5 L 152.5 0.5 C 155.5 0.5 155.5 0.5 155.5 3.5 L 155.5 40.5 C 155.5 43.5 155.5 43.5 152.5 43.5 L 82.5 43.5 76.5 49.5 70.5 43.5 3.5 43.5 C 0.5 43.5 0.5 43.5 0.5 40.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" stroke="#000000" stroke-opacity="0.15" stroke-width="1" transform="translate(1, 1)"></path>
                        <path fill="rgba(247,247,247,0.85)" class="highcharts-label-box highcharts-tooltip-box" d="M 3.5 0.5 L 152.5 0.5 C 155.5 0.5 155.5 0.5 155.5 3.5 L 155.5 40.5 C 155.5 43.5 155.5 43.5 152.5 43.5 L 82.5 43.5 76.5 49.5 70.5 43.5 3.5 43.5 C 0.5 43.5 0.5 43.5 0.5 40.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" stroke="#7cb5ec" stroke-width="1"></path>
                        <text x="8" data-z-index="1" style="font-size:12px;color:#333333;cursor:default;fill:#333333;" y="20">
                            <tspan style="font-size: 10px">Monday, Mar 21, 06:00</tspan>
                            <tspan style="fill:#7cb5ec" x="8" dy="15">●</tspan>
                            <tspan dx="0">Hourly data points: </tspan>
                            <tspan style="font-weight:bold" dx="0">6.66</tspan>
                        </text>
                    </g>
                </svg>
            </div>
            <script type="text/javascript">
			/*
                function getData(n) {
                    var arr = [], i, x, a, b, c, spike;
                    for (i = 0,
                    x = Date.UTC(new Date().getUTCFullYear(), 0, 1) - n * 36e5; i < n; i = i + 1,
                    x = x + 36e5) {
                        if (i % 100 === 0) {
                            a = 2 * Math.random();
                        }
                        if (i % 1000 === 0) {
                            b = 2 * Math.random();
                        }
                        if (i % 10000 === 0) {
                            c = 2 * Math.random();
                        }
                        if (i % 50000 === 0) {
                            spike = 10;
                        } else {
                            spike = 0;
                        }
                     //   console.log(x); // x axis
                     //   console.log(2 * Math.sin(i / 100) + a + b + c + spike); //y axis
                       arr.push([x, 2 * Math.sin(i / 100) + a + b + c + spike + Math.random()]);
                       // arr.push([x, 2 * Math.sin(i / 100) + a + b + c + spike]);
                    }
                    console.log('arr:'+arr)
                    return arr;
                }*/
                function getData() {
                    var arr = [], i, x, a, b, c, spike;
                    i=0;
                    <c:forEach items="${statsValues}" var="statsValues">
                   /* statArray.push([Date.UTC(new Date("${statsValues.dateTime}").getUTCFullYear(), 0, 1),${statsValues.value}]);*/
                   var date=new Date('${statsValues.dateTime}');
                  // console.log('date:'+date);
                   
 						x = Date.UTC(date.getUTCFullYear(), date.getMonth(), date.getDate(), date.getHours(),date.getMinutes(),date.getSeconds());                 
                   	 arr.push([x, ${statsValues.value}]);
                    </c:forEach>
                   // console.log('arr: '+arr);
                    return arr;
                }
                   /* 
                    for (i = 0,
                    x = Date.UTC(new Date().getUTCFullYear(), 0, 1) - n * 36e5; i < n; i = i + 1,
                    x = x + 36e5) {
                        if (i % 100 === 0) {
                            a = 2 * Math.random();
                        }
                        if (i % 1000 === 0) {
                            b = 2 * Math.random();
                        }
                        if (i % 10000 === 0) {
                            c = 2 * Math.random();
                        }
                        if (i % 50000 === 0) {
                            spike = 10;
                        } else {
                            spike = 0;
                        }
                     //   console.log(x); // x axis
                     //   console.log(2 * Math.sin(i / 100) + a + b + c + spike); //y axis
                       arr.push([x, 2 * Math.sin(i / 100) + a + b + c + spike + Math.random()]);
                       // arr.push([x, 2 * Math.sin(i / 100) + a + b + c + spike]);
                    }
                    console.log('arr:'+arr)
                    return arr;
                }*/
                
               // var n = 2
              // alert("OK");
               statArray=[];
               var n=<c:out value="${arrayLength}"/>
                  , data = getData();
                //  data=<c:out value="${listOfStats}"/>;
                  console.log('n: '+n);
			//console.log('data: '+data);
			var parsedList=[];
			/*
		//	var parsedList= JSON.parse(${listOfStats});
		//	console.log('JSON data'+parsedList);
               // var list=message;
            //	console.log('list: '+list);
            	//var parsedList= JSON.parse(message);
            	//console.log('parsedlist: '+parsedList);
//<c:forEach items="${statsValues}" var="statsValues">
//statArray.push([Date.UTC(new Date("${statsValues.dateTime}").getUTCFullYear(), 0, 1),${statsValues.value}]);
//</c:forEach>*/
//console.log('statArray: '+statArray);
                
                console.time('line');
                Highcharts.chart('hc-container', {

                    chart: {
                        zoomType: 'x'
                    },

                    title: {
                        text: '${viewedSensor.type} ( ${viewedSensor.metricSystem} )'
                    },

                    subtitle: {
                        text: ''
                    },

                    tooltip: {
                        valueDecimals: 2
                    },

                    xAxis: {
                        type: 'datetime'
                    },
                   

                    series: [{
                        data: data,
                        lineWidth: 0.5,
                        name: '${viewedSensor.type}'
                    }]

                });
                console.timeEnd('line');
            </script>
        </div>
 