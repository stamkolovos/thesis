package com.thesis.wsn.webUI.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.stereotype.Service;

import com.thesis.wsn.webUI.entities.MonitoredSensors;
import com.thesis.wsn.webUI.entities.SensorStatsFormat;
import com.thesis.wsn.webUI.repository.MonitoredSensorsRepos;

@Service
public class MonitoredSensorsImpl implements MonitoredSensorsService {
	
	@Autowired
	MonitoredSensorsRepos monitoredSensorRepository;
	
	public MonitoredSensors saveSensorValue(MonitoredSensors monitoredSensor) {
	
		
		
		return monitoredSensorRepository.save(monitoredSensor);	
		
		
	}
	
	public List<MonitoredSensors> getSensorValuesBySensorId(int sensorId){
		return monitoredSensorRepository.findValuesBySensorId(sensorId);
	}
	


	



	
}
