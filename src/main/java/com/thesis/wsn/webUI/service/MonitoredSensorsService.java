package com.thesis.wsn.webUI.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.query.Param;

import com.thesis.wsn.webUI.entities.MonitoredSensors;
import com.thesis.wsn.webUI.entities.SensorStatsFormat;

public interface MonitoredSensorsService {

	public MonitoredSensors saveSensorValue(MonitoredSensors monitoredSensorValues);
	
	
	public List<MonitoredSensors> getSensorValuesBySensorId(int sensorId);

}
