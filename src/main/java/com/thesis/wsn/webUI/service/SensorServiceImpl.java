package com.thesis.wsn.webUI.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thesis.wsn.webUI.entities.Sensor;
import com.thesis.wsn.webUI.repository.SensorRepository;


@Service
public class SensorServiceImpl implements SensorService {
	
	@Autowired
	private SensorRepository sensorRepository;
	
	public SensorRepository getSensorRepository() {
		return sensorRepository;
	}

	public void setSensorRepository(SensorRepository sensorRepository) {
		this.sensorRepository = sensorRepository;
	}

	public Sensor saveSensor(Sensor sensor) {
		return  sensorRepository.save(sensor);	
	}
	
	public Sensor updateSensor(Sensor sensor) {
		return sensorRepository.save(sensor);		
	}
	
	public void deleteSensor(Sensor sensor) {
		sensorRepository.delete(sensor);		
	}
	public Sensor getSensorById(int id) {
		return sensorRepository.findById(id).orElse(null);
		
		
	}
	public List<Sensor> getAllSensors(){
		return sensorRepository.findAll();
		
	}
}
