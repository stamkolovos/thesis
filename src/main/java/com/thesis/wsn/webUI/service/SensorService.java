package com.thesis.wsn.webUI.service;

import java.util.List;

import com.thesis.wsn.webUI.entities.Sensor;

public interface SensorService {
	Sensor saveSensor(Sensor sensor);
	Sensor updateSensor(Sensor sensor);
	void deleteSensor(Sensor sensor);
	Sensor getSensorById(int id);
	List<Sensor> getAllSensors();
	
}
