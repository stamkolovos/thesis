package com.thesis.wsn.webUI;

import org.apache.ibatis.type.MappedTypes;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import com.thesis.wsn.webUI.entities.MonitoredSensors;

import com.thesis.wsn.webUI.entities.SensorStatsFormat;

@SpringBootApplication
@EnableScheduling
@Configuration

public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}
