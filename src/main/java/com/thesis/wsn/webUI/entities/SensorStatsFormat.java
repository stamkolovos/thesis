package com.thesis.wsn.webUI.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Table;

//@Embeddable
public class SensorStatsFormat {
	
	//@Column(name="Date_Time")
	public  Date dateTime;
	
	//@Column(name="Value")
	public  int monthInterval;
	public int sensorId;
	
	SensorStatsFormat(Date Date_Time, int Value){
		this.dateTime=dateTime;
		this.monthInterval=monthInterval;
	}
	
	Date getDateTime() {
		return this.dateTime;
	}
	
	int getValue() {
		return this.monthInterval;
	}

	public int getSensorId() {
		return sensorId;
	}

	public void setSensorId(int sensorId) {
		this.sensorId = sensorId;
	}
	
	
	
	//gia to interface
//	public Date getDateTime();
//	public int getValue();
	
}
