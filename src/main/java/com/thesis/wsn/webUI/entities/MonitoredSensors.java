package com.thesis.wsn.webUI.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="monitored_sensors")
public class MonitoredSensors {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private int id;
	
	private String IPv6Address;
	
	@Column(name="Port")
	private int port;
	
	@Column(name="Type")
	private String type;
	
	@Column(name="Metric_System")
	private String metricSystem;
	
	@Column(name="Value")
	private int value;
	
	@Column(name="Location")
	private String location;
	
	@Column(name="Date_Time")
	public Date dateTime;
	
	@Column(name="Sensor_Id")
	private int sensorId;
	
	@Column(name="Status")
	private String status;
	
	public MonitoredSensors() {
		
		
		
	}
	public MonitoredSensors(Sensor sensor) {
		IPv6Address=sensor.getIPv6Address();
		port=sensor.getPort();

		type=sensor.getType();
		metricSystem=sensor.getMetricSystem();
		location=sensor.getLocation();
		sensorId=sensor.getId();
		
	}
	public MonitoredSensors(Date dateTime, int value) {
		this.dateTime=dateTime;
		this.value=value;
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIPv6Address() {
		return IPv6Address;
	}
	public void setIPv6Address(String iPv6Address) {
		IPv6Address = iPv6Address;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMetricSystem() {
		return metricSystem;
	}
	public void setMetricSystem(String metricSystem) {
		this.metricSystem = metricSystem;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Date getDateTime() {
		return dateTime;
	}
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	public int getSensorId() {
		return sensorId;
	}
	public void setSensorId(int sensorId) {
		this.sensorId = sensorId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	@Override
	public String toString() {
		return "MonitoredSensorValues [id=" + id + ", IPv6Address=" + IPv6Address + ", type=" + type + ", metricSystem="
				+ metricSystem + ", value=" + value + ", location=" + location + ", dateTime=" + dateTime + "]";
	}
	
	
}
