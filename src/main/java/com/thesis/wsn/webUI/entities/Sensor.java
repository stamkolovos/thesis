package com.thesis.wsn.webUI.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="sensors")
public class Sensor {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private int id;	
	private String IPv6Address;
	
	@Column(name="Type")
	private String type;
	
	@Column(name="Metric_System")
	private String metricSystem;
	
	@Column(name="Location")
	private String location;
	
	@Column(name="Resource_Path")
	private String resourcePath;
	
	@Column(name="Port")
	private int port;
	
	public Sensor() {
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIPv6Address() {
		return IPv6Address;
	}
	public void setIPv6Address(String iPv6Address) {
		IPv6Address = iPv6Address;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMetricSystem() {
		return metricSystem;
	}
	public void setMetricSystem(String metricSystem) {
		this.metricSystem = metricSystem;
	}
	public String getResourcePath() {
		return resourcePath;
	}
	public void setResourcePath(String resourcePath) {
		this.resourcePath = resourcePath;
	}
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	@Override
	public String toString() {
		return "Sensor [id=" + id + ", IPv6Address=" + IPv6Address + ", type=" + type + ", metricSystem=" + metricSystem
				+ ", resourcePath=" + resourcePath + "]";
	}
	
}
