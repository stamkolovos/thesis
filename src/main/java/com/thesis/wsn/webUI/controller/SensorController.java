package com.thesis.wsn.webUI.controller;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.eclipse.californium.core.CoapClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.thesis.wsn.webUI.entities.MonitoredSensors;
import com.thesis.wsn.webUI.entities.Sensor;
import com.thesis.wsn.webUI.entities.SensorStatsFormat;
import com.thesis.wsn.webUI.repository.MonitoredSensorsRepos;
import com.thesis.wsn.webUI.scheduler.ScheduleSensorValues;
import com.thesis.wsn.webUI.service.MonitoredSensorsService;
import com.thesis.wsn.webUI.service.SensorService;




@Controller
public class SensorController {

	
	
	@Autowired
	SensorService sensorService;
	
	@Autowired
	MonitoredSensorsService monitoredSensorsService;
	
	@Autowired
	MonitoredSensorsRepos repos;
	
	
	@RequestMapping("/")
	public String index() {
	   
	
	    return "index";
	}
	
	@RequestMapping("displaySensors")
	public String displaySensors(ModelMap modelMap) {
		List<Sensor> allSensors = sensorService.getAllSensors();
		modelMap.addAttribute("allSensors",allSensors);
		return "displaySensorsPage";
	}
	
	
	@RequestMapping("addSensor")
	public String addSensor(ModelMap modelMap) {
		
		return "addSensorPage";	
		}
	
	@RequestMapping("/saveSensor")
	public String saveSensor(@ModelAttribute("Sensor") Sensor sensor,ModelMap modelMap) {
		Sensor savedSensor = sensorService.saveSensor(sensor);
		String savedMessage="Sensor was saved with id: "+savedSensor.getId();
		modelMap.addAttribute("savedMessage",savedMessage);
		return "addSensorPage";		
	}
	
	
	
	@RequestMapping("viewSensor")
	public String viewSensor(@RequestParam("id") int sensorId, @RequestParam("statsFor") int monthsInterval,ModelMap modelMap) {
		Sensor viewedSensor=sensorService.getSensorById(sensorId);	
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		
		cal.add(Calendar.MONTH, (-1)*monthsInterval); 
		Date dateInterval = cal.getTime();
		System.out.println("dateInterval: "+dateInterval);
		
		List<MonitoredSensors> stat=repos.findValuesBySensorIdAndDateInterval(viewedSensor.getId(),dateInterval);
		System.out.println("query stat: "+ stat);
		modelMap.addAttribute("viewedSensor",viewedSensor);			
		modelMap.addAttribute("arrayLength",4);
		modelMap.addAttribute("statsValues",stat);
		modelMap.addAttribute("monthsInterval",monthsInterval);
		return "viewSensorPage";
	}
	@RequestMapping("viewSensorAttr")
	public String viewSensorAttr(@ModelAttribute("SensorStatsFormat") SensorStatsFormat sensorStatsFormat,ModelMap modelMap ) {
		int monthInterval=sensorStatsFormat.sensorId;
		System.out.println("monthInterval"+monthInterval);
		return "viewSensorPage";
	}
	
	
	
	@RequestMapping("viewSensorStats")
	public String viewSensor(@RequestParam("id") int sensorId, @RequestParam("per") String statsPer,ModelMap modelMap) {
		
		List<MonitoredSensors> sensorStats= monitoredSensorsService.getSensorValuesBySensorId(sensorId);
		
		SimpleDateFormat ft=new SimpleDateFormat("hh:mm:ss, E dd.MM.yyyy");
		
		
		//decide which values to forward based on the "per" indicator
		
		for(MonitoredSensors mS :sensorStats) {
			System.out.println("Time:"+ft.format(mS.getDateTime()));
			System.out.println("mS.getDateTime: "+mS.getDateTime());
		}
		
		return "viewSensorStatsPage";
	}
	
	@RequestMapping("deleteSensor")
	public String deleteSensor(@RequestParam("id") int id,ModelMap modelMap) {
		Sensor deletedSensor=new Sensor();
		deletedSensor.setId(id);
		sensorService.deleteSensor(deletedSensor);
		List<Sensor> allSensors = sensorService.getAllSensors();
		modelMap.addAttribute("allSensors",allSensors);
		return "displaySensorsPage";
	}
	
	@RequestMapping("editSensor")
	public String editSensor(@RequestParam("id") int id,ModelMap modelMap) {
		Sensor editedSensor=sensorService.getSensorById(id);		
		modelMap.addAttribute("editedSensor", editedSensor);
		return "editSensorPage";
	}
	
	
	
	
	@RequestMapping("monitorSensors")
	
		public String monitorSensors() {
		
		return "monitorSensorsPage";
	}

	
	
}