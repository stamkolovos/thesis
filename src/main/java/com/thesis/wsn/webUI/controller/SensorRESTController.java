package com.thesis.wsn.webUI.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.thesis.wsn.webUI.service.MonitoredSensorsService;
import com.thesis.wsn.webUI.service.SensorService;
import com.thesis.wsn.webUI.entities.MonitoredSensors;
import com.thesis.wsn.webUI.entities.Sensor;
import com.thesis.wsn.webUI.repository.MonitoredSensorsRepos;
import com.thesis.wsn.webUI.scheduler.ScheduleSensorValues;


@RestController
public class SensorRESTController {
	@Autowired
	SensorService sensorService;
	
	@Autowired
	MonitoredSensorsService monitoredSensorsService;

	@Autowired
	MonitoredSensorsRepos repos;
	
	
	@GetMapping("/sensors")
	public List<Sensor> getSensors(){			
		return sensorService.getAllSensors();
	} 
	@GetMapping("/sensors/{id}")
	public Sensor getSensorById(@PathVariable("id") int id) {
		
		return sensorService.getSensorById(id);
	}
	
	
	@PostMapping("/sensors")
	public void addSensor(@RequestBody Sensor sensor) {
		sensorService.saveSensor(sensor);
		
	}
	@PutMapping("/sensors")
	public void updateSensor(@RequestBody Sensor sensor) {
		sensorService.saveSensor(sensor);
		
	}
	
	@DeleteMapping("/sensors/{id}")
	public void deleteSensor(@PathVariable("id")int id) {
		Sensor deletedSensor=sensorService.getSensorById(id);
		sensorService.deleteSensor(deletedSensor);		
		
	}

	//attempt to make the api with parameters

	/*
	@RequestMapping(value="/sensors",method = RequestMethod.GET)
	public Sensor getSensorsByLocation(@RequestParam(value="location")int id) {
		return sensorService.getSensorById(id);
		
	}*/

	
	
	@GetMapping("/monitoredSensors")
	public List<MonitoredSensors> getMonitoredSensors(){
		return ScheduleSensorValues.listOfSensorValues;
		
	}
	/*
	@GetMapping("/monitoredSensors/{sensorId}")
	public List<MonitoredSensors> getMonitoredSensorsbySensorId(@PathVariable("sensorId") int sensorId){
		
		return monitoredSensorsService.getSensorValuesBySensorId(sensorId);
	}*/
	/*
	@RequestMapping(value="/monitoredSensors",method = RequestMethod.GET)
	public List<MonitoredSensors> getMonitoredSensorsBySensorId(@RequestParam(value="sensorId")int sensorId) {
		return monitoredSensorsService.getSensorValuesBySensorId(sensorId);
		
	}*/
	/*
	@RequestMapping(value="/sensorStats/",method = RequestMethod.GET)
	public List<MonitoredSensors> getAllSensorStats(@RequestParam(value="monthInterval")int monthInterval) {
		return monitoredSensorsService.getSensorValuesBySensorId(monthInterval);
		
	}*/
	@RequestMapping(value="/sensorStats",method = RequestMethod.GET)
	public List<MonitoredSensors> getSensorStatsByMonthInterval(@RequestParam(value="monthInterval",defaultValue="1") String monthIntervalStr) {
		int monthInterval=Integer.parseInt(monthIntervalStr);
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		
		cal.add(Calendar.MONTH, (-1)*monthInterval);
		Date dateInterval = cal.getTime();
			
		return repos.findValuesByDateInterval(dateInterval);
	}
	@RequestMapping(value="/sensorStats/{sensorId}",method = RequestMethod.GET)
	public List<MonitoredSensors> getSensorStatsBySensorIdAndMonthInterval(@PathVariable("sensorId") int sensorId,@RequestParam(value="monthInterval",defaultValue="1") String monthIntervalStr) {
		int monthInterval=Integer.parseInt(monthIntervalStr);
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		
		cal.add(Calendar.MONTH, (-1)*monthInterval);
		Date dateInterval = cal.getTime();
			
		return repos.findSensorValuesBySensorIdAndDateInterval(sensorId,dateInterval);
	}
	
	
	
}
