package com.thesis.wsn.webUI.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.thesis.wsn.webUI.entities.Sensor;

public interface SensorRepository extends JpaRepository<Sensor, Integer> {

}
