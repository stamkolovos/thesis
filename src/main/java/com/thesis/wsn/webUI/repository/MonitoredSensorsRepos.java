package com.thesis.wsn.webUI.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.thesis.wsn.webUI.entities.MonitoredSensors;
import com.thesis.wsn.webUI.entities.SensorStatsFormat;

public interface MonitoredSensorsRepos extends JpaRepository<MonitoredSensors, Integer> {

	@Query(
			value="SELECT * FROM monitored_sensors ms WHERE ms.Sensor_Id=:sensorId ",nativeQuery=true)
	public List<MonitoredSensors> findValuesBySensorId(@Param("sensorId") int sensorId);
	
	
	
	//return MonitoredSensor object list with null the other parametres
	@Query("SELECT new com.thesis.wsn.webUI.entities.MonitoredSensors(dateTime,value) FROM MonitoredSensors ms WHERE sensorId=:sensorId AND dateTime>=:dateInterval")	
	public List<MonitoredSensors> findValuesBySensorIdAndDateInterval(@Param("sensorId") int sensorId,@Param("dateInterval") Date dateInterval);	
	
	@Query("SELECT ms FROM MonitoredSensors ms WHERE dateTime>=:dateInterval")
	public List<MonitoredSensors> findValuesByDateInterval(@Param("dateInterval") Date dateInterval);
	
	@Query("SELECT ms FROM MonitoredSensors ms WHERE sensorId=:sensorId AND dateTime>=:dateInterval")	
	public List<MonitoredSensors> findSensorValuesBySensorIdAndDateInterval(@Param("sensorId") int sensorId,@Param("dateInterval") Date dateInterval);
	
}
