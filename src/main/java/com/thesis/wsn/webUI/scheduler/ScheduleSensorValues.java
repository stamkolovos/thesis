package com.thesis.wsn.webUI.scheduler;

import org.springframework.stereotype.Component;

import com.thesis.wsn.webUI.entities.MonitoredSensors;
import com.thesis.wsn.webUI.entities.Sensor;
import com.thesis.wsn.webUI.service.MonitoredSensorsService;
import com.thesis.wsn.webUI.service.SensorService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.eclipse.californium.core.CoapClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;

@Component
public class ScheduleSensorValues {

	@Autowired 
	SensorService sensorService;
	
	@Autowired
	MonitoredSensorsService monitoredSensorsService;
	
	public static List<MonitoredSensors>  listOfSensorValues;
	
	@Autowired
	private SimpMessagingTemplate simpMessagingTemplate;
	
	public ScheduleSensorValues() {
		
	}
	

	
	@Scheduled(fixedDelay=10000 ,initialDelay = 10000)
	public void saveSensorValues() {
		System.out.println("~~~~~~~MONITOR~~~~~~");
		List<Sensor> allSensors=sensorService.getAllSensors();
		listOfSensorValues=new ArrayList<MonitoredSensors>();
		final long intervalPingPongms=400;

		for(Sensor sensor:allSensors) {
			MonitoredSensors monitoredSensor=new MonitoredSensors(sensor);
			
			CoapClient client=new CoapClient("coap://["+sensor.getIPv6Address()+"]:"+sensor.getPort()+sensor.getResourcePath());

			//loop for checking if the device is active
			if(!client.ping(intervalPingPongms)) {
				//inactive sensor
				monitoredSensor.setDateTime(new Date());
				//System.out.println(new Date());				
				monitoredSensor.setStatus("INACTIVE");
				
			}else {
			//active sensor
				
				int sensorValue=Integer.parseInt(client.get().getResponseText()); //string to int
				
				monitoredSensor.setValue(sensorValue);
				monitoredSensor.setDateTime(new Date());
				System.out.println(new Date());
				monitoredSensor.setStatus("ACTIVE");
				monitoredSensorsService.saveSensorValue(monitoredSensor);
				}
		
			
			//h lista me tis teleutaies apothikeumenes times
			ScheduleSensorValues.listOfSensorValues.add(monitoredSensor);
			
			
		}
		
		//send to the subscribers the list of Devices 
		simpMessagingTemplate.convertAndSend("/topic/greetings",ScheduleSensorValues.listOfSensorValues);
	}
	
	
	public static List<MonitoredSensors> getListOfSensorValues() {
		return listOfSensorValues;
		
	}

	

	

	
}



